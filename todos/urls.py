from django.urls import path
from .views import (
    TodoListView,
    TodoListDeleteView,
    TodoListDetail,
    TodoItemCreate,
    TodoItemUpdate,
    TodoListCreate,
    TodoListUpdate,
)

urlpatterns = [
    path("", TodoListView.as_view(), name="todo_list_list"),
    path("<int:pk>/", TodoListDetail, name="todo_list_detail"),
    path("create/", TodoListCreate, name="todo_list_create"),
    path("<int:pk>/edit/", TodoListUpdate, name="todo_list_update"),
    path(
        "<int:pk>/delete/",
        TodoListDeleteView.as_view(),
        name="todo_list_delete",
    ),
    path("items/create/", TodoItemCreate, name="todo_item_create"),
    path("items/<int:pk>/edit/", TodoItemUpdate, name="todo_item_update"),
]
