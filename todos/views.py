from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from .models import TodoList, TodoItem
from .forms import TodoItemForm, TodoListForm


def TodoItemCreate(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        id = request.POST["list"]
        if form.is_valid:
            form.save()
        return redirect("todo_list_detail", id)
    else:
        form = TodoItemForm()
        context = {"form": form}
        return render(request, "items/create.html", context)


def TodoItemUpdate(request, pk):
    old = TodoItem.objects.get(pk=pk)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=old)
        id = request.POST["list"]
        if form.is_valid:
            form.save()
        return redirect("todo_list_detail", id)
    else:
        form = TodoItemForm(instance=old)
        context = {"form": form}
        return render(request, "items/edit.html", context)


def TodoListCreate(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid:
            new = form.save()
        return redirect("todo_list_detail", new.id)
    else:
        form = TodoListForm()
        context = {"form": form}
        return render(request, "todos/create.html", context)


def TodoListUpdate(request, pk):
    old = TodoList.objects.get(pk=pk)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=old)
        if form.is_valid:
            form.save()
        return redirect("todo_list_detail", pk)
    else:
        form = TodoListForm(instance=old)
        context = {"form": form}
        return render(request, "todos/edit.html", context)


def TodoListDetail(request, pk):
    todolist = TodoList.objects.get(pk=pk)
    todoitems = TodoItem.objects.filter(list=pk)
    context = {
        "todoitems": todoitems,
        "todolist": todolist,
    }
    return render(request, "todos/detail.html", context)


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list_list")


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list_update")


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todo_list_list")
